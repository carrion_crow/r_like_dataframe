from typing import List
import numpy as np

class Vector(list):
    def __init___(self, *args, **kw):
        list.__init__(self, *args, **kw)

    def __lt__(self, other):
        if isinstance(other, (int,float)):
            ret = Vector([x < other for x in self])
            return ret
        elif isinstance(other, (list,tuple)):
            if len(self) != len(other):
                raise Exception
            return [s_elem < o_elem for s_elem, o_elem in zip(self, other)]
        else:
            raise Exception
    
    def __le__(self, other):
        if isinstance(other, (int,float)):
            ret = Vector([x <= other for x in self])
            return ret
        elif isinstance(other, (list,tuple)):
            if len(self) != len(other):
                raise Exception
            return [s_elem <= o_elem for s_elem, o_elem in zip(self, other)]
        else:
            raise Exception

    def __ne__(self, other):
        if isinstance(other, (int,float,str)):
            ret = Vector([x != other for x in self])
            return ret
        elif isinstance(other, (list,tuple)):
            if len(self) != len(other):
                raise Exception
            return [s_elem != o_elem for s_elem, o_elem in zip(self, other)]            
        else:
            raise Exception

    def __eq__(self, other) -> List[bool]:
        if isinstance(other, (int,float,str)):
            ret = Vector([x == other for x in self])
            return ret
        elif isinstance(other, (list,tuple)):
            if len(self) != len(other):
                raise Exception
            return [s_elem == o_elem for s_elem, o_elem in zip(self, other)]
        else:
            raise Exception

    def __gt__(self, other):
        if isinstance(other, (int,float)):
            ret = Vector([x > other for x in self])
            return ret
        elif isinstance(other, (list,tuple)):
            if len(self) != len(other):
                raise Exception
            return [s_elem > o_elem for s_elem, o_elem in zip(self, other)]
        else:
            raise Exception

    def __ge__(self, other):
        if isinstance(other, (int,float)):
            ret = Vector([x >= other for x in self])
            return ret
        elif isinstance(other, (list,tuple)):
            if len(self) != len(other):
                raise Exception
            return [s_elem >= o_elem for s_elem, o_elem in zip(self, other)]
        else:
            raise Exception
    
    def is_all_bool(self) -> bool:
        ret = all([isinstance(x, bool) for x in self])
        return ret

def c(*args) -> Vector:
    '''
    return args vector
    (arg elements should be int, float, str, list or tuple)
    '''
    s_class = (int, float, str, np.generic)
    iter_class   = (list, tuple)

    def allsingle_value(inputs):
        '''
        Check whether inputs are all single value or not.
        '''
        if isinstance(inputs, s_class):
            return True
        else:
            ret = [not isinstance(x, s_class) for x in inputs]
            return all(ret)

    def cc(inputs):
        '''
        expand and connect inputs
        '''
        li = []
        for x in inputs:
            if allsingle_value(x):
                if isinstance(x, s_class):
                    li.append(x)
                elif isinstance(x, iter_class):
                    li.extend(x)
                else:
                    raise Exception
            else:
                if isinstance(x, s_class):
                    li.append(x)
                elif isinstance(x, iter_class):
                    li.extend(cc(x))
                else:
                    raise Exception
        return li
    return Vector(cc(args))

if __name__ == "__main__":
    li = Vector([1,2,3])
    print(li)
    re = li < 3
    print(re)
    print(re.is_all_bool)

    nest = c(1,c(2,3),c(4, c(5,6,7)))
    print(nest)
    print(nest > 3)
    str_vector = c("alpha", "bravo", "charlie")
    print(str_vector)
    print(str_vector == "alpha")
    print(c([100,50,"新宿区"]))
    print(c("新宿区"))
    print(c(1))
    print(c(1,2,3) == c(1,2,1))

    
    

