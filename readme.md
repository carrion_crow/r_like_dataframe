# 概要　/ About

R ユーザーにとっては pandas.DataFrame の操作が難しすぎるので、R dataframe 風のインターフェイスを提供します

For R users, pandas.DataFrame operation is too difficult.
This module provide R like datframe interface for python users.

# 使い方　/ Usage

Rのデータフレーム風のデータアクセスができます。代入はまだ作っていません。添え字は　0 origin です

We are implimenting setter method now. Index is 0 origin. 

|都道府県|人口|面積|首都|
|---|---|---|---|
|東京|100|50|新宿区|
|千葉|80|40|千葉市|
|埼玉|60|30|さいたま市|

```python
print(rdf)
#        人口  面積     首都
# 都道府県                
# 東京    100  50    新宿区
# 千葉     80  40    千葉市
# 埼玉     60  30  さいたま市

print(rdf["東京",:])
# [100, 50, '新宿区']

print(rdf[:,"人口"])
# [100, 80, 60]

print(rdf[1,1])
# 40

print(rdf[:,(0,1)])
print(rdf[:,0:1])
#        人口  面積
# 都道府県         
# 東京    100  50
# 千葉     80  40
# 埼玉     60  30

print(rdf[0:1,:])
print(rdf[(0,1),:])
#        人口  面積   首都
# 都道府県              
# 東京    100  50  新宿区
# 千葉     80  40  千葉市

print(rdf[-1,:])
#        人口  面積     首都
# 都道府県                
# 東京    100  50    新宿区
# 埼玉     60  30  さいたま市

print(rdf[:,-1])
#        人口  面積
# 都道府県         
# 東京    100  50
# 千葉     80  40
# 埼玉     60  30

print(rdf[(-1, -2),:])
# [100, 50, '新宿区']

print(rdf[:, (-1, -2)])
# [100, 80, 60]


idx = rdf[:,"人口"] >= 80
print(idx)
# [True, True, False]

print(rdf[idx,:])
#        人口  面積   首都
# 都道府県              
# 東京    100  50  新宿区
# 千葉     80  40  千葉市

idx = rdf[:,"首都"] == "新宿区"
print(idx)
# [True, False, False]

print(rdf[idx,:])
# [100, 50, '新宿区']
```

# License

T.B.D.
