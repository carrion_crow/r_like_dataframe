import unittest
import pandas as pd
from pandas.testing import assert_frame_equal
from rdf import Rdf
from vector import Vector, c

class RdfTest(unittest.TestCase):
    '''
    test case of r_like_dataframe.py
    '''
    def test_get_col_by_number(self):
        tokyo = Rdf.tokyo()
        actual = tokyo[:,0]
        expected = c([100,80,60])
        self.assertEqual(actual, expected)
        actual = tokyo[:,2]
        expected = c(["新宿区","千葉市","さいたま市"])
        self.assertEqual(actual, expected)
        with self.assertRaises(Exception):
            actual = tokyo[:,3]

    def test_get_col_by_name(self):
        tokyo = Rdf.tokyo()
        actual = tokyo[:,0]
        expected = c([100,80,60])
        self.assertEqual(actual, expected)
        actual = tokyo[:,2]
        expected = c(["新宿区","千葉市","さいたま市"])
        self.assertEqual(actual, expected)
        with self.assertRaises(Exception):
            actual = tokyo[:,3]

    def test_get_row_by_number(self):
        tokyo = Rdf.tokyo()
        actual = tokyo[0,:]
        expected = c([100, 50, "新宿区"])
        self.assertEqual(actual, expected)
        actual = tokyo[2,:]
        expected = c([60, 30, "さいたま市"])
        self.assertEqual(actual, expected)
        with self.assertRaises(Exception):
            actual = tokyo[3,:]
    
    def test_get_row_by_numbers(self):
        tokyo = Rdf.tokyo()
        actual = tokyo[(0,1),:]
        expect = pd.read_csv("./test/tokyo-row2.csv", header=0, index_col=0)
        assert_frame_equal(actual.to_pandas(), expect)
        actual = tokyo[0:1,:]
        assert_frame_equal(actual.to_pandas(), expect)


    def test_get_row_by_name(self):
        tokyo = Rdf.tokyo()
        actual = tokyo["東京",:]
        expected = [100, 50, "新宿区"]
        self.assertEqual(actual, expected)
        actual = tokyo["埼玉",:]
        expected = [60, 30, "さいたま市"]
        self.assertEqual(actual, expected)
        with self.assertRaises(Exception):
            actual = tokyo["茨城",:]
    
    def test_get_cell_by_numbers(self):
        tokyo = Rdf.tokyo()
        actual = tokyo[0,0]
        expected = 100
        self.assertEqual(actual, expected)
        actual = tokyo[2,0]
        expected = 60
        self.assertEqual(actual, expected)
        actual = tokyo[0,2]
        expected = "新宿区"
        self.assertEqual(actual, expected)
        actual = tokyo[2,2]
        expected = "さいたま市"
        self.assertEqual(actual, expected)
        with self.assertRaises(Exception):
            actual = tokyo[0,3]
        with self.assertRaises(Exception):
            actual = tokyo[3,0]
        with self.assertRaises(Exception):
            actual = tokyo[3,3]

    def test_get_cell_by_name(self):
        tokyo = Rdf.tokyo()
        actual = tokyo["東京","人口"]
        expected = c(100)
        self.assertEqual(actual, expected)
        actual = tokyo["埼玉","人口"]
        expected = c(60)
        self.assertEqual(actual, expected)
        actual = tokyo["東京","首都"]
        expected = c("新宿区")
        self.assertEqual(actual, expected)
        actual = tokyo["埼玉","首都"]
        expected = c("さいたま市")
        self.assertEqual(actual, expected)
        with self.assertRaises(Exception):
            actual = tokyo["東京","知事"]
        with self.assertRaises(Exception):
            actual = tokyo["茨城","人口"]
        with self.assertRaises(Exception):
            actual = tokyo["茨城","知事"] 
   
    def test_dim(self):
        iris = Rdf.iris()
        actual = iris.dim
        expected = [150, 5]
        self.assertEqual(actual, expected)

    def test_colnames(self):
        iris = Rdf.iris()
        actual = iris.colnames
        expected = ['Sepal.Length', 'Sepal.Width', 'Petal.Length', 'Petal.Width', 'Species']
        self.assertEqual(actual, expected)


if __name__ == "__main__":
    unittest.main()    


