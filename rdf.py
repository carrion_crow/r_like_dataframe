from typing import Tuple, List, Union
import pandas as pd
from pandas import DataFrame

from vector import Vector, c

class Rdf:
    def __init__(self, df:DataFrame):
        self.df:DataFrame = df
        self.df.reset_index()

    def __str__(self) -> str:
        '''
        return pandas dataframe string
        '''
        return self.df.__str__()
    
    def __getitem__(self, key):
        '''
        return dataframe value with R dataframe like method
        '''
        if len(key) == 2:
            row = key[0]
            col = key[1]
            df1 = self.select_row(self.df, row)
            df2 = self.select_col(df1,     col)
            return self.ret_select(df2)
        else:
            raise Exception

    def ret_select(self,df:DataFrame):
        if isinstance(df, pd.Series):
            #ここには来ないようだ
            print("★")
            return df.tolist()
        num_row = df.shape[0]
        num_col = df.shape[1]
        if num_row > 1 and num_col > 1:
            return Rdf(df.copy())
        if num_row == 1:
            ret = df.iloc[0,:].tolist()
        elif num_col == 1:
            ret = df.iloc[:,0].tolist()
        if len(ret) == 1:
            ret = ret[0]
        return c(ret)

    def select_col(self, df:DataFrame, col):
        if isinstance(col, slice):
            if Rdf.is_all(col):
                return df
            else:
                col_list = self.slice_to_list(df.shape[1], col)
        elif isinstance(col, Vector):
            if col.is_all_bool():
                col_list = self.vector_to_list(df.shape[1], col)
        elif isinstance(col, (int,str)):
            col_list = [col,]
        elif isinstance(col, tuple):
            col_list = list(col)
        elif isinstance(col, list):
            col_list = col
        else:
            raise Exception
        if self.all_string(col_list):
            return self.select_col_core(df,name=col_list)
        elif self.all_int(col_list):
            if self.all_positive(col_list):
                return self.select_col_core(df,index=col_list)
            elif self.all_negative(col_list):
                return self.select_col_core(df,index=col_list,remove=True)
            else:
                raise Exception
        else:
            raise Exception

    def select_col_core(self, df:DataFrame, name:List[str]=None, index:List[int]=None, remove:bool=False):
        if name:
            return df.loc[:,name]
        if index:
            if remove:
                new_index = [x for x in range(0,df.shape[0]) if -x not in index]
            else:
                new_index = index
            return df.iloc[:,new_index]

    def select_row(self, df:DataFrame, row) -> DataFrame:
        if isinstance(row, slice):
            if Rdf.is_all(row):
                return df
            else:
                row_list = self.slice_to_list(df.shape[0], row)
        elif isinstance(row, (int,str)):
            row_list = [row,]
        elif isinstance(row, Vector):
            if row.is_all_bool():
                row_list = self.vector_to_list(df.shape[0], row)
        elif isinstance(row, tuple):
            row_list = list(row)
        elif isinstance(row, list):
            row_list = row
        else:
            raise Exception
        if self.all_string(row_list):
            return self.select_row_core(df,name=row_list)
        elif self.all_int(row_list):
            if self.all_positive(row_list):
                return self.select_row_core(df,index=row_list)
            elif self.all_negative(row_list):
                return self.select_row_core(df,index=row_list,remove=True)
            else:
                raise Exception
        else:
            raise Exception
    
    def select_row_core(self, df:DataFrame, name:List[str]=None, index:List[int]=None, remove:bool=False) -> DataFrame:
        if name:
            return df.loc[name]
        if index:
            if remove:
                new_index = [x for x in range(0,df.shape[0]) if -x not in index]
            else:
                new_index = index
            return df.iloc[new_index,:]

    def all_string(self, li:List) -> bool:
        return all((isinstance(x, str) for x in li))

    def all_int(self, li:List) -> bool:
        return all((isinstance(x, int) for x in li))

    def all_positive(self, li:List[int]) -> bool:
        return all((x >= 0 for x in li))

    def all_negative(self, li:List[int]) -> bool:
        # In this function, -0 is negative
        return all((x <= 0 for x in li))

    def slice_to_list(self, row_num:int, s:slice) -> List[int]:
        '''
        Convert from slice to list of index
        '''
        start = s.start
        stop  = s.stop
        if start is None:
            start = 0
        if stop is None:
            stop = row_num
        ret = [x for x in range(start, stop + 1)]
        return ret

    def vector_to_list(self, row_num:int, v:Vector) -> List[int]:
        '''
        Convert from slice to list of index
        '''
        if len(v) != row_num:
            raise Exception
        ret = [x for x, y in enumerate(v) if y is True]

        return ret



    @staticmethod
    def is_all(s:slice) -> bool:
        '''
        '''
        if s.start is None and s.stop is None:
            return True
        else:
            return False

    @property
    def dim(self) -> List[int]:
        """
        return a list of dataframe size [row, col]
        """
        return list(self.df.shape)
    
    @property
    def rownames(self) -> List[str]:
        '''
        return list of row names
        '''
        return self.df.index.tolist()

    @property
    def colnames(self) -> List[str]:
        '''
        return list of col names
        '''
        return self.df.columns.tolist()

    @classmethod
    def iris(cls):
        '''
        return a sample data of Rdf
        '''
        pandas_df = pd.read_csv("./data/iris.csv", header=0, index_col=None)
        return Rdf(pandas_df)
    
    @classmethod
    def tokyo(cls):
        '''
        return a sample data of Rdf
        '''
        pandas_df = pd.read_csv("./data/tokyo.csv", header=0, index_col=0)
        return Rdf(pandas_df)

    
    def to_pandas(self) -> DataFrame:
        '''
        return a copy of pandas dataframe
        '''
        return self.df.copy()

if __name__ == "__main__":
    pandas_df = pd.read_csv("./data/tokyo.csv", index_col=0)
    #rdf = Rdf(pandas_df)
    rdf = Rdf.tokyo()
    print(rdf)
    print(rdf["東京",:])
    print(rdf[:,"人口"])
    idx = rdf[:,"人口"] >= 80
    print(idx)
    print(rdf[idx,:])

    idx = rdf[:,"首都"] == "新宿区"
    print(idx)
    print(rdf[idx,:])

 

    
